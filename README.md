# css-exercises

live on Codepen:

+ <a href="https://codepen.io/TomaszPieta/pen/paLrqP" target="_blank">css loader</a>
+ <a href="https://codepen.io/TomaszPieta/pen/VQExqP" target="_blank">css before after</a>
+ <a href="https://codepen.io/TomaszPieta/pen/yvRLMo" target="_blank">css robot</a>
+ <a href="#">gradient</a>
+ <a href="#">css keyframe bounce</a>
+ <a href="#">css perspective</a>
+ <a href="https://codepen.io/TomaszPieta/pen/OvZobM" target="_blank">css smooth scroll</a>
+ <a href="#">css triangle</a>
